package hwang.codingame.rift

import scala.util.Random

object Player {
  val POD_PRICE: Int = 20

  case class Zone(zid: Int, source: Int, myId: Int) {
    var owner: Int = -1
    val pods: Array[Int] = Array(0, 0, 0, 0)
    var neighbors: List[Zone] = List.empty
    val initSourceWeight: Double = 0.0
    var othersSourcesOnMe: Double = initSourceWeight

    def updatePod(p0: Int, p1: Int, p2: Int, p3: Int) {
      pods(0) = p0
      pods(1) = p1
      pods(2) = p2
      pods(3) = p3
    }

    def isMine = owner == myId

    def isEnemy = owner != -1 && owner != myId

    def isNeutral = owner == -1

    def hasPod = myPod > 0

    def myPod = pods(myId)

    def enemyPods = pods.sum - myPod

    def aroundAverageSource = neighbors.map(_.source).sum

    def aroundEnemyCount = neighbors.map(_.enemyPods).sum + enemyPods

    def hasEnemyInNeighbors = neighbors.exists(_.enemyPods > 0)

    def hasMoreEnemyInNeighbors = neighbors.exists(_.enemyPods > myPod)

    def hasMoreOrSameEnemyInNeighbors = neighbors.exists(_.enemyPods >= myPod)

    def inBoundary = neighbors.exists(!_.isMine)

    def occupyScore = (1 + aroundEnemyCount) * (1 + sourceWeight)

    def sourceWeight = source + 0.5 * othersSourcesOnMe

    def aroundSourceWeight = sourceWeight + 0.5 * neighbors.map(n => n.sourceWeight).sum

  }

  type Dict = Map[Int, Zone]
  type Graph = Map[Int, List[Int]]
  type BreathHistory = Map[Int, List[Int]]
  type Path = List[Int]
  type Distance = Array[Array[Int]]

  trait Action

  case class Move(pods: Int, from: Int, to: Int) extends Action {
    override def toString: String = pods + " " + from + " " + to
  }

  case class Purchase(pods: Int, zid: Int) extends Action {
    override def toString: String = pods + " " + zid
  }

  case class ActionExecutor(actions: List[Action]) {
    def execute(): Unit = if (actions.isEmpty) println("WAIT") else println(actions.mkString(" "))
  }

  def zoneInGraph(dict: Dict, graph: Graph) = dict.values.filter(z => graph.isDefinedAt(z.zid)).toList

  def findInterest(zones: List[Zone]): List[Zone] = {
    zones.filter(z => z.isMine && z.inBoundary).flatMap(mine => {
      mine.neighbors.filterNot(_.isMine)
    }).distinct
  }

  def separateMyZones(graph: Graph, dict: Dict): Graph = {
    graph.filterKeys(dict.get(_).get.isMine).map({ case (key, links) => (key, links.filter(dict.get(_).get.isMine))})
  }

  def neighborWithHistory(from: Int, history: List[Int], visited: Set[Int], g: Graph): BreathHistory = {
    g.get(from).get.filterNot(visited).map(neighbor => (neighbor, neighbor :: history)).toMap
  }

  def newNeighborsWithHistory(elems: BreathHistory, visited: Set[Int], g: Graph): BreathHistory = {
    elems.flatMap({
      case (elem, history) => neighborWithHistory(elem, history, visited, g)
    })
  }

  def findPath(from: Int, to: Int, graph: Graph): List[Int] = {
    path(to, Map(from -> List.empty), Set.empty, graph)
  }

  def path(to: Int, extend: BreathHistory, visited: Set[Int], g: Graph): List[Int] = {
    if (extend.isDefinedAt(to)) extend.get(to).get.reverse
    else {
      path(to, newNeighborsWithHistory(extend, visited, g), extend.keySet ++ visited, g)
    }
  }

  def updateZone(zonecount: Int, dict: Map[Int, Zone]) {
    for (i <- 0 until zonecount) {
      val Array(zid, ownerid, podsp0, podsp1, podsp2, podsp3) = for (i <- readLine split " ") yield i.toInt
      val zone: Zone = dict.get(zid).get
      zone.owner = ownerid
      zone.updatePod(podsp0, podsp1, podsp2, podsp3)
    }
  }

  def buildDict(zonecount: Int, myId: Int): Map[Int, Zone] = {
    doBuildDict(readZones(zonecount, myId))
  }

  def doBuildDict(zones: List[Zone]): Map[Int, Zone] = {
    zones.map(_.zid).zip(zones).toMap
  }

  def readZones(zonecount: Int, myId: Int): List[Zone] = {
    (for {i <- 0 until zonecount
          Array(zoneid, platinumsource) = for (i <- readLine split " ") yield i.toInt
    } yield Zone(zoneid, platinumsource, myId)).toList
  }

  def buildGraph(linkcount: Int): Graph = {
    doBuildGraph(readLinks(linkcount))
  }

  def doBuildGraph(links: List[(Int, Int)]): Graph = {
    val reversedLinks = links.map(l => (l._2, l._1))
    (links ::: reversedLinks).groupBy(_._1).map { case (key, ls) => (key, ls.map(link => link._2))}
  }

  def readLinks(linkcount: Int): List[(Int, Int)] = {
    (for {
      i <- 0 until linkcount
      Array(zone1, zone2) = for (i <- readLine split " ") yield i.toInt} yield (zone1, zone2)).toList
  }

  def buildDisconnectedGraph(g: Graph, subGraphs: List[Graph] = List.empty): List[Graph] = {
    val oneSubgraph = separate(g)
    val rest = g -- oneSubgraph.keySet
    if (rest.isEmpty) oneSubgraph :: subGraphs else buildDisconnectedGraph(rest, oneSubgraph :: subGraphs)
  }

  def separate(g: Graph, subGraph: Graph = Map.empty): Graph = {
    if (g.isEmpty) subGraph
    else {
      if (subGraph.isEmpty) {
        separate(g.tail, subGraph.updated(g.head._1, g.head._2))
      } else {
        val connectedWithSubGraph = g.filter({ case (key, value) => subGraph.values.exists(_.contains(key))})
        if (connectedWithSubGraph.isEmpty) subGraph
        else {
          separate(g -- connectedWithSubGraph.keySet, subGraph ++ connectedWithSubGraph)
        }
      }
    }
  }

  def setNeighbors(graph: Graph, dict: Dict) {
    graph.foreach({ case (zid, links) =>
      val zone: Zone = dict.get(zid).get
      zone.neighbors = links.flatMap(dict.get)
    })
  }

  def zoneToPurchase: Zone => Purchase = zone => Purchase(1, zone.zid)

  def zoneToMove(from: Zone): Zone => Move = zone => Move(1, from.zid, zone.zid)

  def actionWithAllPods[T](pods: Int, zones: List[Zone], zoneToAction: Zone => T, actions: List[T] = Nil): List[T] = {
    if (pods <= 0 || zones.isEmpty) actions
    else {
      actionWithAllPods(pods - zones.size, zones, zoneToAction, actions ::: zones.takeRight(pods).map(zoneToAction))
    }
  }

  def computeDistanceMatrix(graph: Graph): Array[Array[Int]] = {
    val n = graph.keys.max
    val inf = Int.MaxValue

    // Initialize distance matrix.
    val ds = Array.fill[Int](n + 1, n + 1)(inf)
    for (i <- 1 to n) ds(i)(i) = 0
    for ((i, links) <- graph; j <- links)
      ds(i)(j) = 1

    // Initialize next vertex matrix.
    val ns = Array.fill[Int](n + 1, n + 1)(-1)

    // Here goes the magic!
    for (k <- 1 to n; i <- 1 to n; j <- 1 to n)
      if (ds(i)(k) != inf && ds(k)(j) != inf && ds(i)(k) + ds(k)(j) < ds(i)(j)) {
        ds(i)(j) = ds(i)(k) + ds(k)(j)
        ns(i)(j) = k
      }
    ds
  }

  def main(args: Array[String]) {
    System.err.println("game started")
    val Array(playerCount, myId, zoneCount, linkCount) = for (i <- readLine split " ") yield i.toInt
    val dict: Dict = buildDict(zoneCount, myId)
    System.err.println("build dictionary")

    val graph: Graph = buildGraph(linkCount)
    System.err.println("build graph")

    val matrix: Array[Array[Int]] = computeDistanceMatrix(graph)
    System.err.println("build matrix")

    val disconnectedGraph: List[Graph] = buildDisconnectedGraph(graph)
    System.err.println("build conti")

    disconnectedGraph.foreach(g =>
      setNeighbors(g, dict)
    )
    System.err.println("set neighbors")

    // game loop
    val strategies1v3: List[Strategy] = List(LazyFight(dict, disconnectedGraph), EagerPriorityFight(dict, disconnectedGraph))
    val strategy = playerCount match {
      case 2 => GlobalFight(dict, disconnectedGraph)
      case 3 => EagerPriorityFight(dict, disconnectedGraph)
      case 4 => strategies1v3(Random.nextInt(strategies1v3.size))
    }
    var initilized = false
    System.err.println("before game")
    while (true) {
      val platinum = readInt() // my available Platinum
      updateZone(zoneCount, dict)
      System.err.println("after zone update")
      disconnectedGraph.foreach(g =>
        updateSourceWeightWithoutMine(g, dict, matrix)
      )
      System.err.println("after weight update")

      val newPods: Int = platinum / POD_PRICE
      if (initilized) {
        strategy.play(newPods)
        System.err.println("after play")
      }

      else {
        strategy.init(newPods)
        System.err.println("after init")

        initilized = true
      }
    }
  }

  def updateSourceWeightWithoutMine(graph: Graph, dict: Dict, matrix: Array[Array[Int]]) {
    updateZoneSourceWeight(graph, dict.filterNot(_._2.isMine), matrix)
  }

  def updateSourceWeightFromTo(zoneI: Zone, zoneJ: Zone, distance: Int) {
    zoneI.othersSourcesOnMe += zoneJ.source * Math.pow(0.5, distance)
  }

  def updateZoneSourceWeight(graph: Graph, dict: Dict, matrix: Distance) {
    dict.filter(entry => graph.isDefinedAt(entry._1)).values.foreach(z => z.othersSourcesOnMe = z.initSourceWeight)
    for (i <- dict.keys; j <- dict.keys; if i > j && graph.isDefinedAt(i) && graph.isDefinedAt(j)) {
      val zoneI = dict.get(i).get
      val zoneJ = dict.get(j).get
      val distance = matrix(i)(j)
      updateSourceWeightFromTo(zoneI, zoneJ, distance)
      updateSourceWeightFromTo(zoneJ, zoneI, distance)
    }
  }

  trait Strategy {
    val dict: Dict
    val graphs: List[Graph]

    def init(newPods: Int): Unit = {
      ActionExecutor(List.empty).execute()
      ActionExecutor(playInitialPurchase(newPods, dict.values.toList)).execute()
      System.err.println("after init play")

    }

    def play(newPods: Int): Unit = {
      ActionExecutor(playMove()).execute()
      System.err.println("after move")
      ActionExecutor(playPurchase(newPods, dict.values.toList)).execute()
      System.err.println("after purchase")

    }

    def playInitialPurchase(pods: Int, zones: List[Zone]): List[Purchase]

    def playMove(): List[Move] = {
      graphs.flatMap(graph => {
        buildDisconnectedGraph(separateMyZones(graph, dict)).flatMap(myConnectedGraph => {
          val zonesInGraph: List[Zone] = zoneInGraph(dict, myConnectedGraph)
          playPods(zonesInGraph, graph)
        })
      })
    }

    def playPods(zones: List[Zone], graph: Graph): List[Move] = {
      playBoundaryPods(zones) ::: playIdlePods(zones, graph)
    }


    def playBoundaryPods(zones: List[Zone]): List[Move] = {
      zones.filter(z => z.hasPod && z.inBoundary).flatMap(from => {
        doPlayBoundaryPods(from)
      })
    }

    def playIdlePods(zones: List[Zone], graph: Graph): List[Move] = {
      val interests = findInterest(zones)
      zones.filter(from => from.hasPod && !from.inBoundary).flatMap(from => {
        val reachable = interests.map(to => (to, findPath(from.zid, to.zid, graph)))
        if (reachable.isEmpty) Nil
        else {
          val by: List[(Zone, List[Int])] = reachable.sortBy({ case (to, path) => to.occupyScore / (1f + path.size)})
          val prioritized: List[Zone] = by
            .flatMap({ case (to, path) => dict.get(path.head)})
          actionWithAllPods(from.myPod, prioritized, zoneToMove(from))
        }
      })
    }

    def doPlayBoundaryPods(from: Zone): List[Move] = {
      val neighbors: List[Zone] = from.neighbors.filterNot(_.isMine).sortBy(_.occupyScore)
      if (!from.hasMoreEnemyInNeighbors) {
        actionWithAllPods(from.myPod, neighbors, zoneToMove(from))
      } else {
        playBoundaryPodsWithEnemy(from, neighbors)
      }
    }

    def playBoundaryPodsWithEnemy(from: Zone, dests: List[Zone]): List[Move] = {
      actionWithAllPods(from.myPod - from.neighbors.map(_.enemyPods).max, dests, zoneToMove(from))
    }

    def playPurchase(pods: Int, zones: List[Zone]): List[Purchase] = {
      val defencePurchases: List[Purchase] = playDefendPurchase(pods, zones)
      if (defencePurchases.size < pods) {
        defencePurchases ::: playInvestPurchase(pods - defencePurchases.size, zones)
      } else defencePurchases
    }

    def playDefendPurchase(pods: Int, zones: List[Zone]): List[Purchase] = {
      val defenceZones: List[Zone] = zones.filter(z => z.isMine && z.inBoundary && z.hasMoreEnemyInNeighbors).sortBy(_.source)
      actionWithAllPods(pods.min(defenceZones.size * 3), defenceZones, zoneToPurchase)
    }

    def playInvestPurchase(pods: Int, zones: List[Zone]): List[Purchase] = {
      val neutrals: List[Zone] = zones.filter(z => z.isNeutral)
      if (neutrals.nonEmpty) {
        actionWithAllPods(pods, neutrals.sortBy(z => z.sourceWeight), zoneToPurchase)
      }
      else {
        val myBoundaryZones: List[Zone] = zones.filter(z => z.isMine && z.inBoundary)
        actionWithAllPods(pods, myBoundaryZones, zoneToPurchase)
      }
    }

    def findInitialPurchaseZones(remainingZones: Set[Zone], purchaseZones: Set[Zone] = Set.empty): Set[Zone] = {
      if (remainingZones.isEmpty) purchaseZones
      else {
        val maxSourceZone = remainingZones.maxBy(_.aroundSourceWeight)
        findInitialPurchaseZones(remainingZones -- maxSourceZone.neighbors - maxSourceZone, purchaseZones + maxSourceZone)
      }
    }

    def findMostValueableZones(zones: List[Zone]): List[Zone] = {
      findInitialPurchaseZones(zones.filter(_.isNeutral).sortBy(_.source).takeRight(10).toSet).toList.sortBy(_.aroundSourceWeight)
    }
  }

  case class GlobalFight(dict: Dict, graphs: List[Graph]) extends Strategy {
    def playInitialPurchase(pods: Int, zones: List[Zone]): List[Purchase] = {
      actionWithAllPods(pods, findMostValueableZones(zones).takeRight(6), zoneToPurchase)
    }
  }

  case class LazyFight(dict: Dict, graphs: List[Graph]) extends Strategy {
    val globalFighter = GlobalFight(dict, graphs)
    val sortedBySource: List[Graph] = graphs.sortBy(graph => zoneInGraph(dict, graph).map(_.source).sum)

    var played = false
    var priorityZones: List[Zone] = Nil
    var otherZones: List[Zone] = Nil

    override def playPurchase(pods: Int, zones: List[Zone]): List[Purchase] = {
      if (!played) {
        played = true
        priorityZones = zoneInGraph(dict, findPriorityGraph) ::: graphs.filter(g => zoneInGraph(dict, g).forall(_.isNeutral)).flatMap(zoneInGraph(dict, _))
        otherZones = (zones.toSet -- priorityZones.toSet).toList
        globalFighter.playInitialPurchase(pods, priorityZones)
      } else {
        val purchase: List[Purchase] = globalFighter.playPurchase(pods, priorityZones)
        val consumedPods = purchase.map(_.pods).sum
        purchase ::: (if (pods > consumedPods) globalFighter.playPurchase(pods - consumedPods, otherZones) else List.empty)
      }
    }

    def findPriorityGraph = {
      sortedBySource.takeRight(3).maxBy(graph => {
        val zones = zoneInGraph(dict, graph)
        val resources = zones.filter(_.isNeutral).map(_.source).sum
        val enemyPower = zones.filter(_.isEnemy).map(z => z.enemyPods * POD_PRICE + z.source * 10).sum
        resources / (1f + enemyPower)
      })
    }

    override def playInitialPurchase(pods: Int, zones: List[Zone]): List[Purchase] = {
      Nil
    }
  }

  case class EagerPriorityFight(dict: Dict, graphs: List[Graph]) extends Strategy {
    val globalFight = GlobalFight(dict, graphs)
    val totalSize = dict.size.toFloat
    val totalSource = dict.values.map(_.source).sum.toFloat
    val sortedByPriority: List[Graph] = graphs.sortBy(graph => {
      val sourceWeight: Float = zoneInGraph(dict, graph).map(_.source).sum / totalSource
      val sizeWeight: Float = graph.size / totalSize
      sourceWeight / (1f + 2 * sizeWeight)
    })
    var priorityZones = zoneInGraph(dict, sortedByPriority.last)
    val otherZones = sortedByPriority.reverse.tail.flatMap(g => zoneInGraph(dict, g))

    override def playPurchase(pods: Int, zones: List[Zone]): List[Purchase] = {
      val emptyGraphs: List[Graph] = graphs.filter(g => zoneInGraph(dict, g).forall(_.isNeutral))
      val purchases: List[Purchase] = emptyGraphs.flatMap(g => globalFight.playInvestPurchase(1, zoneInGraph(dict, g)))
      purchases ::: globalFight.playPurchase(pods - emptyGraphs.size, zones)
    }

    override def playInitialPurchase(pods: Int, zones: List[Zone]): List[Purchase] = {
      globalFight.playInitialPurchase(pods, priorityZones)
    }
  }
}