package hwang.codingame.rift

import hwang.codingame.rift.Player._
import org.scalatest.FunSuite

class Player$Test extends FunSuite {
  val myId = 0
  val enemy1 = 1

  /**
   * 1 - 2
   */
  test("test distance matrix") {
    assertResult(1)(computeDistanceMatrix(doBuildGraph(List((1, 2))))(1)(2))
  }

  /**
   * 4
   * |
   * 1 - 2 - 3
   */
  test("test distance two step matrix") {
    val matrix: Array[Array[Int]] = computeDistanceMatrix(doBuildGraph(List((1, 2), (2, 3), (1, 4))))
    assertResult(2)(matrix(1)(3))
    assertResult(1)(matrix(1)(2))
    assertResult(1)(matrix(3)(2))
    assertResult(2)(matrix(4)(2))
  }

  /**
   * 8(1) - 9(1) - 10(1)
   *
   */
  test("zone source weight") {
    val graph = doBuildGraph(List((8, 9), (9, 10)))
    val dict = buildDict(graph)
    val matrix = computeDistanceMatrix(graph)
    updateZoneSourceWeight(graph, dict, matrix)
    assertResult(1)(dict.get(9).get.othersSourcesOnMe)
    assertResult(0.75)(dict.get(10).get.othersSourcesOnMe)
    assertResult(0.75)(dict.get(8).get.othersSourcesOnMe)
  }

  /**
   * 8(1) - 9(1)
   * 1(1) - 2(1)
   *
   */
  test("zone source weight without disconnected graphs") {
    val graph = doBuildGraph(List((8, 9), (1, 2)))
    val dict = buildDict(graph)
    val matrix = computeDistanceMatrix(graph)
    buildDisconnectedGraph(graph).foreach(g =>
      updateSourceWeightWithoutMine(g, dict, matrix)
    )
    assertResult(0.5)(dict.get(1).get.othersSourcesOnMe)
    assertResult(0.5)(dict.get(2).get.othersSourcesOnMe)
    assertResult(0.5)(dict.get(8).get.othersSourcesOnMe)
    assertResult(0.5)(dict.get(9).get.othersSourcesOnMe)
  }

  test("create action with all pods on an empty zone ") {
    assertResult(List.empty)(actionWithAllPods(1, List.empty, zoneToPurchase))
  }

  test("create action with all pods when pods are not enough ") {
    val zones = List(Zone(1, 2, myId), Zone(2, 3, myId))
    assertResult(List(Purchase(1, 2)))(actionWithAllPods(1, zones, zoneToPurchase))
  }

  test("create action with all pods when pods are extra") {
    val zones = List(Zone(1, 2, myId), Zone(2, 3, myId))
    assertResult(List(Purchase(1, 1), Purchase(1, 2), Purchase(1, 2)))(actionWithAllPods(3, zones, zoneToPurchase))
  }

  test("build graph") {
    assertResult(Map(1 -> List(2, 3), 2 -> List(1), 3 -> List(1)))(doBuildGraph(List((1, 2), (1, 3))))
  }

  test("separate graph") {
    assertResult(Map(1 -> List(2), 2 -> List(1)))(separate(Map(1 -> List(2), 2 -> List(1), 3 -> List(4), 4 -> List(3))))
  }

  test("disconnected graph") {
    assertResult(List(Map(1 -> List(2), 2 -> List(1)), Map(3 -> List(4), 4 -> List(3))).reverse)(buildDisconnectedGraph(
      Map(1 -> List(2), 2 -> List(1), 3 -> List(4), 4 -> List(3))))
  }

  test("disconnected graph more hierarchy") {
    val one = Map(1 -> List(2), 4 -> List(3), 2 -> List(1, 3), 3 -> List(2, 4))
    assertResult(one)(separate(one))
  }

  /**
   * 1(mine) - 2(mine) - 4(neutral)
   * |
   * 3(mine)
   */
  test("build connected graph on my zones") {
    val graph = doBuildGraph(List((1, 2), (1, 3), (2, 4)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    ownByMe(dict, 2)
    ownByMe(dict, 3)
    assertResult(doBuildGraph(List((1, 2), (1, 3))))(separateMyZones(graph, dict))
  }

  /**
   * 1(mine) - 2(mine) - 4(neutral)- 5(mine) - 6(mine)
   * |
   * 3(mine)
   */
  test("build multiple connected graph on my zones ") {
    val graph = doBuildGraph(List((1, 2), (1, 3), (2, 4), (4, 5), (5, 6)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    ownByMe(dict, 2)
    ownByMe(dict, 3)
    ownByMe(dict, 5)
    ownByMe(dict, 6)
    assertResult(List(doBuildGraph(List((5, 6))), doBuildGraph(List((1, 2), (1, 3)))).reverse)(buildDisconnectedGraph(separateMyZones(graph, dict)))
  }

  test("one self path") {
    val graph = Map(1 -> List(2), 2 -> List(1))
    assertResult(List())(path(1, Map(1 -> List.empty), Set.empty, graph))
  }

  test("one step path") {
    val graph = Map(1 -> List(2), 2 -> List(1))
    assertResult(List(2))(path(2, Map(1 -> List.empty), Set.empty, graph))
  }

  /**
   * 1 - 2 - 3
   */
  test("two step path") {
    val graph = Map(1 -> List(2), 2 -> List(1, 3), 3 -> List(1, 2))
    assertResult(List(2, 3))(path(3, Map(1 -> List.empty), Set.empty, graph))
  }

  /**
   * 1 - 2 - 3
   * |
   * 4
   */
  test("two step path with a noisy") {
    val graph = Map(1 -> List(2, 4), 2 -> List(1, 3), 3 -> List(1, 2), 4 -> List(1))
    assertResult(List(2, 3))(path(3, Map(1 -> List.empty), Set.empty, graph))
  }

  test("play move") {
    val graph = doBuildGraph(List((1, 2)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    dict.get(1).get.pods(myId) = 1
    assertResult(List(Move(1, 1, 2)))(GlobalFight(dict, List(graph)).playPods(dict.values.toList, graph))
  }

  /**
   * 1(n) - 2(my) - 3(my) - 4(my) - 5(my) - 6(n)
   */
  test("Idle pod should move to boundary zone where enemy is detected") {
    val graph = doBuildGraph(List((1, 2), (2, 3), (3, 4), (4, 5), (5, 6)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 2)
    ownByMe(dict, 3)
    ownByMe(dict, 4)
    ownByMe(dict, 5)
    updateSourceWeightWithoutMine(graph, dict, computeDistanceMatrix(graph))
    dict.get(3).get.pods(myId) = 1
    dict.get(6).get.pods(myId + 1) = 1
    assertResult(List(Move(1, 3, 4)))(GlobalFight(dict, List(graph)).playPods(dict.values.toList, graph))
  }

  /**
   * 1(my) - 2(my) - 3(neutral)
   */
  test("All Idle pods should move to boundary zone ") {
    val graph = doBuildGraph(List((1, 2), (2, 3)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    ownByMe(dict, 2)
    dict.get(1).get.pods(myId) = 2
    assertResult(List(Move(1, 1, 2), Move(1, 1, 2)))(GlobalFight(dict, List(graph)).playPods(dict.values.toList, graph))
  }

  /**
   * 3(neutral) -1(mine) - 2(neutral)
   */
  test("pod should be purchased on my zone if enemy is detected aside") {
    val graph = doBuildGraph(List((1, 2), (1, 3)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    dict.get(2).get.pods(myId + 1) = 1
    assertResult(List(Purchase(1, 1)))(GlobalFight(dict, List(graph)).playPurchase(1, dict.values.toList))
  }

  /**
   * 2(neutral) -1(mine)
   */
  test("pod should be purchased on neutral zone if there is no enemy") {
    val graph = doBuildGraph(List((1, 2)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    assertResult(List(Purchase(1, 2)))(GlobalFight(dict, List(graph)).playPurchase(1, dict.values.toList))
  }

  /**
   * 2(neutral) -1(mine) - 3(enemy)
   */
  test("pod should be moved separately if they are enough for attack and defense and occupy") {
    val graph = doBuildGraph(List((1, 2), (1, 3)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    ownByEnemy1(dict, 3)
    dict.get(1).get.pods(myId) = 3
    dict.get(3).get.pods(enemy1) = 1
    assertResult(List(Move(1, 1, 2), Move(1, 1, 3), Move(1, 1, 3)))(GlobalFight(dict, List(graph)).playPods(dict.values.toList, graph))
  }

  /**
   * 1(mine) - 3(enemy)
   * |       /
   * 2(mine)
   */
  test("neighbor friend zone should not be considered as enemy") {
    val graph = doBuildGraph(List((1, 2), (2, 3), (3, 1)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    ownByMe(dict, 2)
    ownByEnemy1(dict, 3)
    dict.get(1).get.pods(myId) = 3
    dict.get(2).get.pods(myId) = 2
    dict.get(3).get.pods(enemy1) = 1
    assertResult(List(Move(1, 2, 3), Move(1, 2, 3), Move(1, 1, 3), Move(1, 1, 3), Move(1, 1, 3)))(GlobalFight(dict, List(graph)).playPods(dict.values.toList, graph))
  }

  /**
   * 1(mine) - 2(enemy)
   */
  test("all necessary pods should be moved to enemy") {
    val graph = doBuildGraph(List((1, 2)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    ownByEnemy1(dict, 2)
    dict.get(1).get.pods(myId) = 3
    dict.get(2).get.pods(enemy1) = 1
    assertResult(List(Move(1, 1, 2), Move(1, 1, 2), Move(1, 1, 2)))(GlobalFight(dict, List(graph)).playPods(dict.values.toList, graph))
  }

  /**
   * 1(mine) - 2(neutral)
   */
  test("all necessary pods should be moved to neutral") {
    val graph = doBuildGraph(List((1, 2)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    dict.get(1).get.pods(myId) = 3
    assertResult(List(Move(1, 1, 2), Move(1, 1, 2), Move(1, 1, 2)))(GlobalFight(dict, List(graph)).playPods(dict.values.toList, graph))
  }

  /**
   * 1(mine) - 2(neutral)
   */
  test("event my zone without source should be protected") {
    val graph = doBuildGraph(List((1, 2)))
    val dict = Map(1 -> Zone(1, 0, myId), 2 -> Zone(2, 1, myId))
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    ownByEnemy1(dict, 2)
    dict.get(2).get.pods(enemy1) = 1
    assertResult(List(Purchase(1, 1)))(GlobalFight(dict, List(graph)).playDefendPurchase(1, dict.values.toList))
  }

  /**
   * 1(mine) - 2(mine)
   */
  test("no purchase should be done on a safe graph") {
    val graph = doBuildGraph(List((1, 2)))
    val dict = buildDict(graph)
    setNeighbors(graph, dict)
    ownByMe(dict, 1)
    ownByMe(dict, 2)
    assertResult(List.empty)(GlobalFight(dict, List(graph)).playPurchase(1, dict.values.toList))
  }

  def ownByMe(dict: Dict, zid: Int) {
    dict.get(zid).get.owner = myId
  }

  def ownByEnemy1(dict: Dict, zid: Int) {
    dict.get(zid).get.owner = enemy1
  }

  def buildDict(graph: Graph): Dict = {
    graph.map(entry => (entry._1, Zone(entry._1, 1, myId)))
  }
}
