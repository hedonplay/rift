#Purchase
## Initial purchase
###1v1
Recar 3 2 2 1

Initial purchase is extremely important
Zone in a high source density region should be prioritized
Disconnected graph with high source should be prioritized

Waiting one round is not necessary the good choice for three players.

## Following purchase

### Defense purchase

### Invest purchase
Zones having the same source but less neighbors should be prioritized.

Grouped SG zone should be prioritized

Apart from initial purchases, extra one can be done when remaining platinium are enough for potential defence.


#Defence
Defense should not be considered when no enemy is detected.

One should put at least one POD on its zones once enemy is detected aside.

Idle pod should move to boundary zone where enemy is detected

#Occupy
Occupy should take place as long as enemy is absent.

Occupy should not take place when enemy has more pods. (Try to surround an enemy SG zone)

#Disconnected zones
Pods should be played in disconnected zones. A zone belonging to a zone should not be played in another disconnected zone.

#Multiple players
SG zone having biggest source should be avoided because others will also put focus on it.
The primary goal in this mode is to keep his pods safe. For example, don't confront more than one player in the meantime.
